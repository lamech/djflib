A personal library of shell chicanery by Dan Friedman. Mangle to taste. Patches welcome.

License: [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html)

#!/bin/bash -v

perl -e"'$1' =~ /.mp4$/ or die 'Argument $1 is not an mp4 file??'" || exit 1

FILE=`basename "$1" .mp4`
ffmpeg -i "$1" -c:a copy -vn -sn "${FILE}.m4a"

#export PATH=$PATH:/Users/dan/Library/Python/2.7/bin:/Users/dan/bin
export PATH=$PATH:/Users/dan/Library/Python/3.7/bin:/Users/dan/bin
export PATH="/usr/local/opt/mysql-client/bin:$PATH"
export PATH="/Applications/LilyPond.app/Contents/Resources/bin:$PATH"
export PATH="$PATH:/Users/dan/consensus/bin"

export ICLOUD_PATH="~/Library/Mobile Documents/com~apple~CloudDocs/"

export PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

export PERL5LIB="$HOME/perl/lib/perl5/darwin-thread-multi-2level:$HOME/perl/lib/perl5"

# configure bc:
export BC_ENV_ARGS=~/.bcrc

# color output:
alias ls='ls -G'
alias ll='ls -lG'
alias cvlc='vlc -I rc'

alias logicprefsbackup='cp ~/Library/Preferences/com.apple.logic* ~/icloud/Documents/Logic/'

# setalarm 5
# setalarm 5 'Remember to do the thing!'
# setalarmq 5 'Remember to do the thing!' # don't announce up front out loud
QUIET=0
setalarm() {
  ALERT_MSG=${2:-"Times up!"}
  TIME=`date "+%l:%M"`
  ALERT_TIME=`date -v+$1M "+%l:%M"`

  ANNOUNCE_MSG="It's $TIME. I will say '$ALERT_MSG' in $1 minutes at $ALERT_TIME."
  echo $ANNOUNCE_MSG
  if [[ $QUIET == '0' ]]
  then
    say $ANNOUNCE_MSG
  fi
  sleep $(echo "$1 * 60" | bc)
  for x in $(seq 1000); do 
    TIME=`date "+%l %M"`
    say "It's $TIME. $ALERT_MSG"
    sleep 15
  done
}

setalarmq() {
  export QUIET=1
  setalarm $1 "$2"
}
set -o vi

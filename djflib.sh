#!/bin/bash

LGREEN='\033[38;5;46m'
BOLD='\033[1m'
RESET='\033[0m'

function w {
    echo -e "${LGREEN}${BOLD}${1}${RESET}"
}

#!/bin/bash -v

cd ~/bin
cp youtubedown /tmp
curl -O -H "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36" https://www.jwz.org/hacks/youtubedown
chmod 755 youtubedown

youtubedown && rm /tmp/youtubedown
